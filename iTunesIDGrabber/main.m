//
//  main.m
//  iTunesIDGrabber
//
//  Created by Chris Niven on 2019-06-18.
//  Copyright © 2019 BILL JOBS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ScriptingBridge/ScriptingBridge.h>
#import "iTunes.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...

        iTunesApplication *iTunes = [SBApplication applicationWithBundleIdentifier:@"com.apple.iTunes"];

		while (true) {
			iTunesTrack *track = [iTunes currentTrack];

			NSString *escapedIdString = [NSRegularExpression escapedPatternForString:[NSString stringWithFormat:@"%@ - %@", [track artist], [track name]]];

			NSLog(@"%@ %@ %@", escapedIdString, [track artist], [track name]);

            NSString *command = [NSString stringWithFormat:@"echo \"%@\" | pdsend 3333 localhost udp", escapedIdString];
            system(command.UTF8String);
            sleep(5);
        }
    }
    return 0;
}
